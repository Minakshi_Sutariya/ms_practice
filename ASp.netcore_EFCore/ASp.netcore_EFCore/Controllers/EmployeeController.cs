﻿using ASp.netcore_EFCore.Data;
using ASp.netcore_EFCore.Repository;
using ASp.netcore_EFCore.ViewModel;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using ReflectionIT.Mvc.Paging;
using System;
using System.Threading.Tasks;
using System.Linq;

namespace ASp.netcore_EFCore.Controllers
{
    public class EmployeeController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IEmployeeRepository EMP_Repo;
        public EmployeeController(IEmployeeRepository em, IMapper mapper)
        {
            this.EMP_Repo = em;
            this._mapper = mapper;
        }

        [Route("Employee")]
        [HttpGet]
        public IActionResult Index()
        {
            var EmployeeList = EMP_Repo.GetEmployee();
            return View(EmployeeList);
        }
        [HttpGet]
        public ActionResult Employee(int id)
        {

            EmployeeViewmodel employeeViewmodel = new EmployeeViewmodel();
            if (id != 0)
            {
                employeeViewmodel = EMP_Repo.GetEmployeebyId(id);
            }
            return View(employeeViewmodel);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Employee(EmployeeViewmodel Model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Employee EmployeeMOdel = _mapper.Map<Employee>(Model);
                    int success = EMP_Repo.AddOrEdit(EmployeeMOdel);
                    if (success == 1)
                        TempData["Message"] = "Added Successfully";
                    else if (success == 2)
                        TempData["Message"] = "Updated Successfully";
                    else
                        TempData["Message"] = "Something Went Wrong";
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["message"] = "something went wrong";
                return null;
            }
        }
        [HttpGet]
        public IActionResult delete(int id)
        {
            try
            {
                if (id <= 0)
                {
                    return NotFound();
                }
                string message = EMP_Repo.Delete(id);
                TempData["message"] = message;
                return RedirectToActionPermanent("Index");
            }
            catch (Exception ex)
            {
                TempData["message"] = "something went wrong";
                return null;
            }
        }
        [HttpGet]
        public IActionResult ViewDetail(int id)
        {
            try
            {
                EmployeeViewmodel employeeViewmodel = new EmployeeViewmodel();

                if (id <= 0) return NotFound();
                employeeViewmodel = EMP_Repo.GetEmployeebyId(id);
                return Json(employeeViewmodel);
            }
            catch (Exception ex)
            {
                TempData["message"] = "something went wrong";
                return null;
            }
        }
    }
}