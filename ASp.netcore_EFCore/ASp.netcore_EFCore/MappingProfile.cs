﻿using ASp.netcore_EFCore.Data;
using ASp.netcore_EFCore.ViewModel;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASp.netcore_EFCore
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<EmployeeViewmodel, Employee>();
        }
    }
}
