﻿using ASp.netcore_EFCore.Data;
using ASp.netcore_EFCore.ViewModel;
using Microsoft.EntityFrameworkCore;
using ReflectionIT.Mvc.Paging;
using System.Collections.Generic;
using System.Linq;

namespace ASp.netcore_EFCore.Repository
{
    public class EmployeeRepository : IEmployeeRepository
    {
        private readonly EmployeedbContext db;
        public EmployeeRepository(EmployeedbContext employeedbContext)
        {
            this.db = employeedbContext;

        }

        public int AddOrEdit(Employee Model)
        {
            if (db != null)
            {
                if (Model.Id == 0)
                {
                    db.Employee.Add(Model);
                    db.SaveChanges();
                    return 1;
                }
                if (Model.Id > 0)
                {
                    db.Employee.Update(Model);
                    db.SaveChanges();
                    return 2;
                }
            }
            return 0;
        }

        public string Delete(int id)
        {
            string message = null;
            var employee=db.Employee.Find(id);
            if(employee!=null)
            {
                db.Employee.Remove(employee);
                db.SaveChanges();
                message = "Deleted Successfully";
            }
            return message; 
        }

        public IEnumerable<EmployeeViewmodel> GetEmployee()
        {
            var employeelist = db.Employee.Select(e => new EmployeeViewmodel
            {
                Name = e.Name,
                Age = e.Age,
                BirthDate = e.BirthDate,
                Username = e.Username,
                Id = e.Id
            }).AsNoTracking().OrderBy(p => p.Id);

            //var model =  PagingList.CreateAsync(employeelist, 1, 1);
            //var pagedResult =  PagingList<EmployeeViewmodel>.CreateAsync(employeelist, 1, 1);
            return employeelist;
        }
        public EmployeeViewmodel GetEmployeebyId(int id)
        {
            var employeeViewmodel = db.Employee.Where(x => x.Id == id).Select(e => new EmployeeViewmodel
            {
                Name = e.Name,
                Age = e.Age,
                BirthDate = e.BirthDate,
                Username = e.Username,
                Id = e.Id,
                Password = e.Password,
                Email = e.Email
            }).FirstOrDefault();
            return employeeViewmodel;
        }
    }
}
