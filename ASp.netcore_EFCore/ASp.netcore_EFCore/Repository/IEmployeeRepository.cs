﻿using ASp.netcore_EFCore.Data;
using ASp.netcore_EFCore.ViewModel;
using ReflectionIT.Mvc.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASp.netcore_EFCore.Repository
{
    public interface IEmployeeRepository
    {
        IEnumerable<EmployeeViewmodel> GetEmployee();
        int AddOrEdit(Employee Model);
        string Delete(int id);
        EmployeeViewmodel GetEmployeebyId(int id);
    }
}
