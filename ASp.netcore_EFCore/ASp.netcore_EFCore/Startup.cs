using ASp.netcore_EFCore.Data;
using ASp.netcore_EFCore.Repository;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using ReflectionIT.Mvc.Paging;

namespace ASp.netcore_EFCore
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAutoMapper(typeof(Startup));
            services.AddMvc();
            services.AddPaging();
            services.AddDbContext<EmployeedbContext>(options =>
            options.UseSqlServer(Configuration.GetConnectionString("EmployeedbConnection")));
            services.AddScoped<IEmployeeRepository, EmployeeRepository>();//Use for Controller dependency injection
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            ///app.use will cal next request pipline and app.run will not call next request pipline
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            //Log file add
            loggerFactory.AddFile("Logs/mylog-{Date}.txt");//for add file need package  Serilog.Extensions.Logging.File

            //Endpoint Routing is a system to handle routing across different middleware systems (e.g. MVC, Razor Pages, Blazor, SignalR and gRPC).
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
