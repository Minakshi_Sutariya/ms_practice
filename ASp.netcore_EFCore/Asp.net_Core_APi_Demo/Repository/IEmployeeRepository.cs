﻿using Asp.net_Core_APi_Demo.MyDataconnection;
using System.Collections.Generic;

namespace Asp.net_Core_APi_Demo.Repository
{
    public interface IEmployeeRepository
    {
        IEnumerable<Employee> GetEmployee();
        Employee GetEmployeebyId(int id);
        string Delete(int id);
        int AddOrEdit(Employee Model);
    }
}
