﻿using Asp.net_Core_APi_Demo.MyDataconnection;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace Asp.net_Core_APi_Demo.Repository
{
    public class EmployeeRepository : IEmployeeRepository
    {
        private readonly EmployeedbContext db;
        public EmployeeRepository(EmployeedbContext employeedbContext)
        {
            this.db = employeedbContext;
        }

        public IEnumerable<Employee> GetEmployee()
        {
            var employeelist = db.Employee.AsEnumerable().OrderBy(p => p.Id);
            return employeelist;
        }
        public Employee GetEmployeebyId(int id)
        {
            var employeeViewmodel = db.Employee.Where(x => x.Id == id).Select(e => new Employee
            {
                Name = e.Name,
                Age = e.Age,
                BirthDate = e.BirthDate,
                Username = e.Username,
                Id = e.Id,
                Password = e.Password,
                Email = e.Email
            }).FirstOrDefault();
            return employeeViewmodel;
        }
        public string Delete(int id)
        {
            string message = null;
            var employee = db.Employee.Find(id);
            if (employee != null)
            {
                db.Employee.Remove(employee);
                db.SaveChanges();
                message = "Deleted Successfully";
            }
            else
                message = "Data not found";
            return message;
        }
        public int AddOrEdit(Employee Model)
        {
            if (db != null)
            {
                if (Model.Id == 0)
                {
                    db.Employee.Add(Model);
                    db.SaveChanges();
                    return 1;
                }
                if (Model.Id > 0)
                {
                    db.Employee.Update(Model);
                    db.SaveChanges();
                    return 2;
                }
            }
            return 0;
        }
    }
}
