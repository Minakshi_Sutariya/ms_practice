﻿using System;
using System.Collections.Generic;

namespace Asp.net_Core_APi_Demo.MyDataconnection
{
    public partial class Employee
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public DateTime? BirthDate { get; set; }
        public string Password { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
    }
}
