﻿using Asp.net_Core_APi_Demo.MyDataconnection;
using Asp.net_Core_APi_Demo.Repository;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace Asp.net_Core_APi_Demo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeTestController : ControllerBase
    {
        private readonly IEmployeeRepository EMP_Repo;
        public EmployeeTestController(IEmployeeRepository e)
        {
            EMP_Repo = e;
        }
        [HttpGet]
        public IEnumerable<Employee> Get()
        {
            return EMP_Repo.GetEmployee();
        }
        [HttpGet("{id}")]
        public Employee EmployeeById(int id)
        {
            Employee employee = new Employee();
            if (id != 0)
            {
                employee = EMP_Repo.GetEmployeebyId(id);
            }
            return employee;
        }
        [HttpDelete]
        public string delete(int id)
        {
            try
            {
                if (id <= 0)
                {
                    return "Not Found";
                }
                string message = EMP_Repo.Delete(id);
                return message;
            }
            catch (Exception ex)
            {
                return "something went wrong";
            }
        }

        [HttpPost]
        public string Create(Employee Model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int success = EMP_Repo.AddOrEdit(Model);
                    if (success == 1)
                        return "Added Successfully";
                    else if (success == 2)
                        return "Updated Successfully";
                    else
                        return "Something Went Wrong";
                }
                return "Something Went Wrong";
            }
            catch (Exception ex)
            {
                return "Something Went Wrong";
            }
        }
    }
}